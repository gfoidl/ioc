| CircleCi | AppVeyor | NuGet |   
| -- | -- | -- |  
| [![CircleCI](https://circleci.com/bb/gfoidl/ioc/tree/master.svg?style=svg)](https://circleci.com/bb/gfoidl/ioc/tree/master) | [![Build status](https://ci.appveyor.com/api/projects/status/f645fkm37ym9ikjt?svg=true)](https://ci.appveyor.com/project/GntherFoidl/ioc) | [![NuGet](https://img.shields.io/nuget/v/gfoidl.IoC.svg?style=flat-square)](https://www.nuget.org/packages/gfoidl.IoC) |  

![dependency-injection-golf.png](https://bitbucket.org/repo/x5zGEM/images/4185566619-dependency-injection-golf.png)