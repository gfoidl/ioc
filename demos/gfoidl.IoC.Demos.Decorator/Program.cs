﻿using System;
using System.Diagnostics;

namespace gfoidl.IoC.Demos.Decorator
{
    public interface IFoo
    {
        void DoIt();
    }
    //-------------------------------------------------------------------------
    public class Foo : IFoo
    {
        public void DoIt() => Console.WriteLine("I'm doing it");
    }
    //-------------------------------------------------------------------------
    public class FooDecorator : IFoo
    {
        private readonly IFoo _foo;
        //---------------------------------------------------------------------
        public FooDecorator(IFoo foo) => _foo = foo;
        //---------------------------------------------------------------------
        public void DoIt()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            _foo.DoIt();
            Console.ResetColor();
        }
    }
    //-------------------------------------------------------------------------
    public interface IWorker
    {
        void Work();
    }
    //-------------------------------------------------------------------------
    public class Worker : IWorker
    {
        private readonly IFoo _foo;
        //---------------------------------------------------------------------
        public Worker(IFoo foo) => _foo = foo;
        public void Work()      => _foo.DoIt();
    }
    //-------------------------------------------------------------------------
    static class Program
    {
        static void Main()
        {
            IoCContainer ioc = new IoCContainer();

            ioc.Register<IFoo, Foo>("base");
            ioc.Register<IFoo, FooDecorator>();
            ioc.Register<IWorker, Worker>();

            //IFoo foo = ioc.Resolve<IFoo>();

            IWorker worker = ioc.Resolve<IWorker>();

            worker.Work();

            if (Debugger.IsAttached)
            {
                Console.WriteLine("\nEnd.");
                Console.ReadKey();
            }
        }
    }
}