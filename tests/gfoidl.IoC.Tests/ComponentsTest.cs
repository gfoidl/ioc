﻿using System;
using System.Collections.Generic;
using System.Linq;
using gfoidl.IoC.Internal;
using NUnit.Framework;

namespace gfoidl.IoC.Tests
{
    [TestFixture]
    public class ComponentsTest
    {
        #region Ctor
        [Test]
        public void Ctor_CountIs0()
        {
            Components sut = new Components();

            Assert.AreEqual(0, sut.ComponentCount);
        }
        #endregion
        //---------------------------------------------------------------------
        #region ContainsName
        [Test]
        public void ContainsName_NoComponentsSet_ReturnsFalse()
        {
            Components sut = new Components();

            Assert.IsFalse(sut.ContainsName("a"));
        }
        //---------------------------------------------------------------------
        [Test]
        [TestCase("a", ExpectedResult = true)]
        [TestCase("b", ExpectedResult = false)]
        public bool ContainsName_ComponentWithName_ReturnsCorrectResult(string name)
        {
            Components sut = new Components();
            sut.AddComponent(new Component(typeof(Foo), "a", false));

            return sut.ContainsName(name);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ContainsName_ComponentWithNoName_ReturnsTrue()
        {
            Components sut = new Components();
            sut.AddComponent(new Component(typeof(Foo), null, false));
            sut.AddComponent(new Component(typeof(Foo), null, false));

            Assert.IsTrue(sut.ContainsName(null));
        }
        #endregion
        //---------------------------------------------------------------------
        #region GetNames
        [Test]
        public void GetNames_NoComponentsSet_ReturnEmptyResult()
        {
            Components sut = new Components();

            IEnumerable<string> actual = sut.GetNames();

            Assert.AreEqual(0, actual.Count());
        }
        //---------------------------------------------------------------------
        [Test]
        public void GetNames_ComponentsRegistered_ReturnsCorrectNames()
        {
            Components sut = new Components();
            sut.AddComponent(new Component(typeof(Foo), "b", false));
            sut.AddComponent(new Component(typeof(Foo), "a", false));

            string[] expected = { "a", "b" };
            string[] actual   = sut.GetNames().ToArray();

            CollectionAssert.AreEquivalent(expected, actual);
        }
        #endregion
        //---------------------------------------------------------------------
        #region AddComponent
        [Test]
        public void AddComponent_OneComponent_CountIncreasesByOne()
        {
            Components sut = new Components();
            int count      = sut.ComponentCount;

            sut.AddComponent(new Component(typeof(Foo), "abc", false));

            Assert.AreEqual(count + 1, sut.ComponentCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void AddComponent_OneComponent_ContainsNameReturnsTrue()
        {
            Components sut = new Components();

            sut.AddComponent(new Component(typeof(Foo), "abc", false));

            Assert.IsTrue(sut.ContainsName("abc"));
        }
        #endregion
        //---------------------------------------------------------------------
        #region GetComponent
        [Test]
        public void GetComponent_ByName_CorrectComponentIsReturned([Values("", "a", "b")]string name)
        {
            Components sut = new Components();
            sut.AddComponent(new Component(typeof(Foo), null, false));
            sut.AddComponent(new Component(typeof(Foo), "b", false));
            sut.AddComponent(new Component(typeof(Foo), "a", false));

            Component actual = sut.GetComponent(name, false);

            Assert.AreEqual(name, actual.Name);
        }
        //---------------------------------------------------------------------
        [Test]
        public void GetComponent_NotThisType_CorrectComponentIsReturned()
        {
            Components sut = new Components(typeof(IDecorFoo));
            sut.AddComponent(new Component(typeof(FooDecorator), null, false));
            sut.AddComponent(new Component(typeof(DecorFoo), null, false));

            Component actual = sut.GetComponent(null, false, typeof(FooDecorator));

            Assert.AreEqual(typeof(DecorFoo), actual.Type);
        }
        //---------------------------------------------------------------------
        [Test]
        public void GetComponent_NotThisTypeAndOnlyOne_ThrowsInvalidOperation()
        {
            Components sut = new Components(typeof(IDecorFoo));
            sut.AddComponent(new Component(typeof(FooDecorator), null, false));

            Assert.Throws<InvalidOperationException>(() =>
            {
                Component actual = sut.GetComponent(null, false, typeof(FooDecorator));
            });
        }
        #endregion
        //---------------------------------------------------------------------
        #region GetEnumerator
        [Test]
        public void GetEnumerator_NoComponentsSet_ReturnsEmptyResult()
        {
            Components sut = new Components();

            int actual = sut.Count();

            Assert.AreEqual(0, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void GetEnumerator_ComponentsRegistered_ReturnsCorrectResult()
        {
            Components sut = new Components();
            sut.AddComponent(new Component(typeof(Foo), "b", false));
            sut.AddComponent(new Component(typeof(Foo), "a", false));

            Assert.AreEqual(2, sut.Count());
            Assert.IsTrue(sut.Any(c => c.Name == "a"));
            Assert.IsTrue(sut.Any(c => c.Name == "b"));
        }
        #endregion
    }
}