﻿using System.Reflection;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
	[TestFixture]
	public class ArgumentsFixture
	{
		[Test]
		public void Resolve_ObjectWithArguments_ReturnsCorrectObject()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IMaster, Master>();

			object slave = new object();

			IMaster master = sut.Resolve<IMaster>(slave);

			Assert.IsInstanceOf<Master>(master);
			Assert.AreSame(slave, master.Slave);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_ObjectWithStringArguments_ReturnsCorrectObject()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IMaster, Emperor>();

			IMaster master = sut.Resolve<IMaster>(new object[] { "Gü" });

			Assert.IsInstanceOf<Emperor>(master);
			Assert.AreEqual("Gü", master.Name);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_ObjectWithArgumentsNoArgumentGiven_ThrowsTargetParameterCount()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IMaster, Master>();

			Assert.Throws<TargetParameterCountException>(() => sut.Resolve<IMaster>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_ObjectWithArgumentsWrongNumberOfArguments_ThrowsTargetParameterCount()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IMaster, Master>();

			object slave = new object();

			Assert.Throws<TargetParameterCountException>(() => sut.Resolve<IMaster>(slave, 42));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_OptionalCtorArgAndArgGiven_ReturnsCorrectType()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IFuzyGuzy, FuzyGuzy>();

			object arg = new object();

			IFuzyGuzy actual = sut.Resolve<IFuzyGuzy>(arg);

			Assert.IsInstanceOf<FuzyGuzy>(actual);
			Assert.AreEqual(arg, (actual as FuzyGuzy).Arg);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_OptionalStringCtorArgAndArgGiven_ReturnsCorrectType()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IFuzyGuzy, FuzyWuzy>();

			IFuzyGuzy actual = sut.Resolve<IFuzyGuzy>(new object[] { "Gü" });

			Assert.IsInstanceOf<FuzyWuzy>(actual);
			Assert.AreEqual("Gü", (actual as FuzyWuzy).Arg);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_OptionalCtorArgAndArgNotGiven_ReturnsCorrectType()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IFuzyGuzy, FuzyGuzy>();

			IFuzyGuzy actual = sut.Resolve<IFuzyGuzy>();

			Assert.IsInstanceOf<FuzyGuzy>(actual);
			Assert.IsNull((actual as FuzyGuzy).Arg);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_OptionalStringCtorArgAndArgNotGiven_ReturnsCorrectType()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IFuzyGuzy, FuzyWuzy>();

			IFuzyGuzy actual = sut.Resolve<IFuzyGuzy>();

			Assert.IsInstanceOf<FuzyWuzy>(actual);
			Assert.AreEqual("yeaaah", (actual as FuzyWuzy).Arg);
		}
	}
}