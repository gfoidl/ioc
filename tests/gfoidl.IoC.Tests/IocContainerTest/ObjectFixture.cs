﻿using System;
using System.IO;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class ObjectFixture
    {
        [Test]
        public void Register_Object_OK()
        {
            IoCContainer sut = new IoCContainer();
            MemoryStream ms  = new MemoryStream();

            sut.Register<IDisposable>(ms);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Register_ObjectWithName_OK()
        {
            IoCContainer sut = new IoCContainer();
            MemoryStream ms  = new MemoryStream();

            sut.Register<IDisposable>(ms, "abcd");
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_ObjectRegistered_ObjectIsReturned()
        {
            IoCContainer sut = new IoCContainer();
            MemoryStream ms  = new MemoryStream();

            sut.Register<IDisposable>(ms);

            var comp = sut.Resolve<IDisposable>();
            Assert.AreSame(ms, comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_ObjectIsRegisteredWithName_CorrectObjectIsReturned()
        {
            IoCContainer sut = new IoCContainer();
            MemoryStream ms1 = new MemoryStream();
            MemoryStream ms2 = new MemoryStream();

            sut.Register<IDisposable>(ms1, "ms1");
            sut.Register<IDisposable>(ms2, "ms2");

            var comp1 = sut.Resolve<IDisposable>("ms1");
            var comp2 = sut.Resolve<IDisposable>("ms2");

            Assert.AreSame(ms1, comp1);
            Assert.AreSame(ms2, comp2);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_TypeWhoNeedsARegisteredObject()
        {
            IoCContainer sut          = new IoCContainer();
            FileSystemAdapter adapter = new FileSystemAdapter();
            sut.Register<IFileSystemAdapter>(adapter);
            sut.Register<IBuildDirectoryStructureService, BuildDirectoryStructureService>();

            var builder = sut.Resolve<IBuildDirectoryStructureService>();

            Assert.AreSame(adapter, builder.FileSystemAdapter);
        }
    }
}