﻿using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class SubTypesFixtureTest
    {
        [Test]
        public void Resolve_ParentInterface_ReturnsCorrectInstance()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IChild, ChildClass>();

            object instance = sut.Resolve<IParent>();

            Assert.IsInstanceOf<ChildClass>(instance);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_ParentClass_ReturnsCorrectInstance()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<ChildClass, ChildClass>();

            object instance = sut.Resolve<ParentClass>();

            Assert.IsInstanceOf<ChildClass>(instance);
        }
    }
}