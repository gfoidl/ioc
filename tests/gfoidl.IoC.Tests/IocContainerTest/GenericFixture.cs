﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class GenericsFixture
    {
        [Test]
        public void Register_GenericType_OK()
        {
            IoCContainer sut = new IoCContainer();
            
            sut.Register(typeof(IList<>), typeof(List<>));

            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_IListInt_ReturnsListInt()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IList<>), typeof(List<>));

            var component = sut.Resolve<IList<int>>();

            Assert.IsInstanceOf<List<int>>(component);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_TwoDifferentGenerics_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IList<>), typeof(List<>));

            var comp1 = sut.Resolve<IList<int>>();
            var comp2 = sut.Resolve<IList<string>>();

            Assert.IsInstanceOf<List<int>>(comp1);
            Assert.IsInstanceOf<List<string>>(comp2);
        }
        //-------------------------------------------------------------------------
        [Test]
        public void Resolve_TypeWithCtorArgumentsRegistered_InstanceIsReturned()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IGeneric<>), typeof(GenericA<>));
            sut.Register(typeof(IFoo<>), typeof(Foo<>));

            var foo     = sut.Resolve<IFoo<string>>();
            var generic = (foo as Foo<string>).Component;

            Assert.IsInstanceOf<Foo<string>>(foo);
            Assert.IsInstanceOf<GenericA<string>>(generic);
        }
        //-------------------------------------------------------------------------
        [Test]
        public void Resolve_2GenericNamedTypes_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IGeneric<>), typeof(GenericA<>), "a");
            sut.Register(typeof(IGeneric<>), typeof(GenericB<>), "b");

            var a = sut.Resolve<IGeneric<int>>("a");
            var b = sut.Resolve<IGeneric<int>>("b");

            Assert.IsInstanceOf<GenericA<int>>(a);
            Assert.IsInstanceOf<GenericB<int>>(b);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_GenericUnnamedTypeResolveMultipleTimes_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IGeneric<>), typeof(GenericA<>));

            var a1 = sut.Resolve<IGeneric<int>>();
            var a2 = sut.Resolve<IGeneric<int>>();

            Assert.IsInstanceOf<GenericA<int>>(a1);
            Assert.IsInstanceOf<GenericA<int>>(a2);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_GenericNamedTypeResolveMultipleTimes_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IGeneric<>), typeof(GenericA<>), "a");

            var a1 = sut.Resolve<IGeneric<int>>("a");
            var a2 = sut.Resolve<IGeneric<int>>("a");

            Assert.IsInstanceOf<GenericA<int>>(a1);
            Assert.IsInstanceOf<GenericA<int>>(a2);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_ConcreteGeneric_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IFoo<>), typeof(FooA), "a");
            sut.Register(typeof(IFoo<>), typeof(FooB), "b");

            var a = sut.Resolve<IFoo<ModelA>>("a");
            var b = sut.Resolve<IFoo<ModelB>>("b");

            Assert.IsInstanceOf<FooA>(a);
            Assert.IsInstanceOf<FooB>(b);
        }
    }
}