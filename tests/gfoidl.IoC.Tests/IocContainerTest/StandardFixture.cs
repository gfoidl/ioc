﻿using System;
using System.IO;
using System.Threading;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
	[TestFixture]
	public class StandardFixture
	{
		[Test]
		public void Default_ReturnsInstanceOfContainer()
		{
			IoCContainer cont = IoCContainer.Default;

			Assert.IsNotNull(cont);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Default_ReturnsSingletonInstanceOfContainer()
		{
			var cont1 = IoCContainer.Default;
			var cont2 = IoCContainer.Default;

			Assert.AreSame(cont1, cont2);
		}
		//---------------------------------------------------------------------
		[Test]
		[Repeat(10)]
		public void Default_ThreadAttack_ReturnsSingletonInstanceOfContainer()
		{
			IoCContainer[] cont = new IoCContainer[2];
			using (CountdownEvent cdeStart = new CountdownEvent(cont.Length))
			using (ManualResetEventSlim mre = new ManualResetEventSlim())
			using (CountdownEvent cdeFinish = new CountdownEvent(cont.Length))
			{
				for (int i = 0; i < cont.Length; ++i)
				{
					int index = i;
					ThreadPool.QueueUserWorkItem(_ =>
					{
						cdeStart.Signal();
						mre.Wait();
						cont[index] = IoCContainer.Default;
						cdeFinish.Signal();
					});
				}
				cdeStart.Wait();
				mre.Set();
				cdeFinish.Wait();
			}

			Assert.AreSame(cont[0], cont[1]);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_Types_OK()
		{
			IoCContainer sut = new IoCContainer();

			sut.Register<IDisposable, MemoryStream>();

			Assert.AreEqual(1, sut.RegisteredContractCount);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_TypeDoesNotImplementInterface_ThrowsInvalidCast()
		{
			IoCContainer sut = new IoCContainer();

			Assert.Throws<InvalidCastException>(() => sut.Register<IMath, Foo>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_TypeIsNotDerivedFromClass_ThrowsInvalidCast()
		{
			IoCContainer sut = new IoCContainer();

			Assert.Throws<InvalidCastException>(() => sut.Register<Parent, Foo>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_NonInterfaceType_TypeIsRegistered()
		{
			IoCContainer sut = new IoCContainer();

			sut.Register<Parent, Child1>();

			Assert.AreEqual(1, sut.RegisteredContractCount);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_ValueType_TypeIsRegistered()
		{
			IoCContainer sut = new IoCContainer();

			sut.Register<IComparable, int>();

			Assert.AreEqual(1, sut.RegisteredContractCount);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_TypeRegistered_CorrectTypeIsReturned()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable, MemoryStream>();

			var component = sut.Resolve<IDisposable>();

			Assert.IsInstanceOf<MemoryStream>(component);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_NonInterfaceType_CorrectTypeIsReturned()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<Parent, Child1>();

			var component = sut.Resolve<Parent>();

			Assert.IsInstanceOf<Child1>(component);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_TypeIsRegisteredNotAsSingleton_NewInstanceIsReturned()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable, MemoryStream>();

			var comp1 = sut.Resolve<IDisposable>();
			var comp2 = sut.Resolve<IDisposable>();

			Assert.That(comp1, Is.Not.SameAs(comp2));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_TypeWithCtorArgumentsRegistered_InstanceIsReturned()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IFileSystemAdapter, FileSystemAdapter>();
			sut.Register<IBuildDirectoryStructureService, BuildDirectoryStructureService>();

			var service = sut.Resolve<IBuildDirectoryStructureService>();
			var adapter = (service as BuildDirectoryStructureService).FileSystemAdapter;

			Assert.IsInstanceOf<BuildDirectoryStructureService>(service);
			Assert.IsInstanceOf<FileSystemAdapter>(adapter);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_NewImplemenationGetsRegisteredForType_NewImplemenationIsReturned()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable, MemoryStream>();

			var comp1 = sut.Resolve<IDisposable>();
			sut.Register<IDisposable, Foo>();
			var comp2 = sut.Resolve<IDisposable>();

			Assert.IsInstanceOf<MemoryStream>(comp1);
			Assert.IsInstanceOf<Foo>(comp2);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_ValueType_ThrowsInvalidOperationBecauseNoCtorIsDefined()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				IoCContainer sut = new IoCContainer();
				sut.Register<IComparable, int>();

				var actual = sut.Resolve<IComparable>();

				Assert.IsInstanceOf<int>(actual);
			});
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_TypeNotRegistered_ThrowsInvalidOperation()
		{
			Assert.Throws<InvalidOperationException>(() =>
			{
				IoCContainer sut = new IoCContainer();

				var comp = sut.Resolve<IDisposable>();
			});
		}
	}
}