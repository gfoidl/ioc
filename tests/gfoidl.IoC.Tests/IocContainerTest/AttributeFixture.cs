﻿using System.Reflection;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class AttributeFixture
    {
        [Test]
        public void RegisterFromAttributes_AssemblyNotGiven_ComponentsRegistered()
        {
            IoCContainer sut = new IoCContainer();

            sut.RegisterFromAttributes();

            Assert.AreEqual(6, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void RegisterFromAttributes_AssemblyGiven_ComponentsRegistered()
        {
            IoCContainer sut = new IoCContainer();

            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            Assert.AreEqual(6, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_StandardType_CorrectTypeReturned()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp = sut.Resolve<IMyFoo>();

            Assert.IsInstanceOf<MyFoo>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_NamedType_CorrectTypeReturned()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp1 = sut.Resolve<IMyMath>("add");
            var comp2 = sut.Resolve<IMyMath>("subtract");

            Assert.IsInstanceOf<MyAdd>(comp1);
            Assert.IsInstanceOf<MySubtract>(comp2);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_InternalTypeAndPublicContract_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp = sut.Resolve<IMyMath>("divide");

            Assert.IsInstanceOf<MyDivide>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_PublicTypeAndInternalContract_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp = sut.Resolve<IMyInternalInterface>("public");

            Assert.IsInstanceOf<MyPublicClass>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_InternalTypeAndInternalContract_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp = sut.Resolve<IMyInternalInterface>("internal");

            Assert.IsInstanceOf<MyInternalClass>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_GenericType_CorrectTypeReturned()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp = sut.Resolve<IMyGeneric<int>>();

            Assert.IsInstanceOf<MyGeneric<int>>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_TypeWithMultipleContracts_OK()
        {
            IoCContainer sut = new IoCContainer();
            sut.RegisterFromAttributes(Assembly.GetExecutingAssembly());

            var comp1 = sut.Resolve<IParent>();
            var comp2 = sut.Resolve<IMath>();

            Assert.IsInstanceOf<MyClassWithMultipleContracts>(comp1);
            Assert.IsInstanceOf<MyClassWithMultipleContracts>(comp2);
        }
    }
}