﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class MultipleComponentsFixture
    {
        #region Register
        [Test]
        public void Register_NamedComponentNameIsNullOrEmpty_OK(
            [Values(null, "", " ")]string name)
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IDisposable, MemoryStream>(name);

            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Register_NamedComponentsForSameContract_OK()
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("subtract");

            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Register_2ComponentsWithSameName_FirstIsOverwritten()
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IMath, Add>("comp");
            sut.Register<IMath, Subtract>("comp");

            Assert.IsInstanceOf<Subtract>(sut.Resolve<IMath>("comp"));
            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Register_2ComponentWithNullOrEmptyName_OverridesFirst(
            [Values(null, "")]string name)
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IMath, Add>(name);
            sut.Register<IMath, Subtract>(name);

            Assert.IsInstanceOf<Subtract>(sut.Resolve<IMath>(name));
            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Register_NamedComponentsForSameContractAndAsSingleton_OK()
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IMath, Add>("add", true);
            sut.Register<IMath, Subtract>("subtract", true);

            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        #endregion
        //---------------------------------------------------------------------
        #region Resolve
        [Test]
        public void Resolve_NameIsNullAndOneComponentHasNullAsName_ReturnsComponentWithNameIsNull()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>(null);

            var comp = sut.Resolve<IMath>(null as string);

            Assert.IsInstanceOf<Subtract>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_NameIsGiven_ReturnsCorrectComponent()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("subtract");

            var comp = sut.Resolve<IMath>("subtract");

            Assert.IsInstanceOf<Subtract>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_NoComponentWithGivenName_ThrowsInvalidOperation()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");

            Assert.Throws<InvalidOperationException>(() => sut.Resolve<IMath>("abcd"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_NamedTypeIsRegisteredAsSingleton_SameInstanceIsReturend()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add", true);

            var comp1 = sut.Resolve<IMath>("add");
            var comp2 = sut.Resolve<IMath>("add");

            Assert.AreSame(comp1, comp2);
        }
        #endregion
        //---------------------------------------------------------------------
        #region GetRegisteredNamesForContract
        [Test]
        public void GetRegisteredNamesForContract_ContractNotFound_ThrowsInvalidOperation()
        {
            IoCContainer sut = new IoCContainer();

            Assert.Throws<InvalidOperationException>(() => sut.GetRegisteredNamesForContract<IDisposable>());
        }
        //---------------------------------------------------------------------
        [Test]
        public void GetRegisteredNamesForContract_NamedComponentsRegistered_ReturnsCorrectNames()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("subtract");

            string[] expected = { string.Empty, "add", "subtract" };
            string[] actual   = sut.GetRegisteredNamesForContract<IMath>().ToArray();

            CollectionAssert.AreEquivalent(expected, actual);
        }
        #endregion
    }
}