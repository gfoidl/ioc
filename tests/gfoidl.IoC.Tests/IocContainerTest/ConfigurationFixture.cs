﻿using System;
using gfoidl.IoC.Configuration;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
	[TestFixture]
	//[Explicit("Configuration file is needed.")]
	public class ConfigurationFixture
	{
		#region Register
		[Test]
		public void RegisterFromConfiguration_Null_ThrownArgumentNull()
		{
			IoCContainer sut = new IoCContainer();

			Assert.Throws<ArgumentNullException>(() => sut.RegisterFromConfiguration(null as IoCConfigurationSection));
		}
		//---------------------------------------------------------------------
		[Test]
		public void RegisterFromConfiguration_ConfigGiven_OK()
		{
			IoCContainer sut = new IoCContainer();

			sut.RegisterFromConfiguration();

			Assert.AreEqual(2, sut.RegisteredContractCount);
		}
		//---------------------------------------------------------------------
		[Test]
		public void RegisterFromConfiguration_SectionNameIsNull_ThrowsArgumentNull()
		{
			IoCContainer sut = new IoCContainer();

			Assert.Throws<ArgumentNullException>(() => sut.RegisterFromConfiguration(null as string));
		}
		//---------------------------------------------------------------------
		[Test]
		public void RegisterFromConfiguration_SectionNameGiven_OK()
		{
			IoCContainer sut = new IoCContainer();

			sut.RegisterFromConfiguration("iocSectionGroup/modules");

			Assert.AreEqual(1, sut.RegisteredContractCount);
		}
		//---------------------------------------------------------------------
		[Test]
		public void RegisterFromConfiguration_AndRegisterFromCode_AllTypesAreRegistered()
		{
			IoCContainer sut = new IoCContainer();

			sut.RegisterFromConfiguration();
			int configTypes = sut.RegisteredContractCount;

			sut.Register<IFoo<int>, Foo<int>>();

			Assert.AreEqual(configTypes + 1, sut.RegisteredContractCount);
		}
		#endregion
		//---------------------------------------------------------------------
		#region Resolve
		[Test]
		public void Resolve_Configuration_OK()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration();

			var add = sut.Resolve<IMath>("add");
			var subtract = sut.Resolve<IMath>("subtract");

			Assert.IsInstanceOf<Add>(add);
			Assert.IsInstanceOf<Subtract>(subtract);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_SectionName_OK()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration("iocSectionGroup/modules");

			var add = sut.Resolve<IMath>("add");
			var subtract = sut.Resolve<IMath>("subtract");

			Assert.IsInstanceOf<Add>(add);
			Assert.IsInstanceOf<Subtract>(subtract);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_GenericAndNamed_OK()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration();

			var a = sut.Resolve<IGeneric<int>>("a");
			var b = sut.Resolve<IGeneric<string>>("b");

			Assert.IsInstanceOf<GenericA<int>>(a);
			Assert.IsInstanceOf<GenericB<string>>(b);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_ConfigurationDefault_ReturnsAdd()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration();

			var comp = sut.Resolve<IMath>();

			Assert.IsInstanceOf<Add>(comp);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_SingletonAdd_ReturnsSameInstance()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration();

			var comp1 = sut.Resolve<IMath>("singletonAdd");
			var comp2 = sut.Resolve<IMath>("singletonAdd");

			Assert.AreSame(comp1, comp2);
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_NameNotRegistered_ThrowsInvalidOperation()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration();

			Assert.Throws<InvalidOperationException>(() => sut.Resolve<IMath>("123456"));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_NameNotRegisteredAndUseDefaultSet_DefaultUsed()
		{
			IoCContainer sut = new IoCContainer();
			sut.RegisterFromConfiguration();

			var comp = sut.Resolve<IMath>("123456", true);

			Assert.IsInstanceOf<Add>(comp);
		}
		#endregion
	}
}