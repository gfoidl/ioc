﻿using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class DecoratorFixture
    {
        [Test]
        public void Componentes_given___Resolve_returns_correct_Type()
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IDecorFoo, DecorFoo>("decorated");
            sut.Register<IDecorFoo, FooDecorator>();
            sut.Register<IDecorWorker, DecorWorker>();

            IDecorWorker worker = sut.Resolve<IDecorWorker>();

            Assert.IsInstanceOf<FooDecorator>(worker.Foo);

            var decorator = worker.Foo as FooDecorator;

            Assert.IsInstanceOf<DecorFoo>(decorator.Foo);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Componentes_given_in_different_Order___Resolve_returns_correct_Type()
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IDecorFoo, FooDecorator>();
            sut.Register<IDecorFoo, DecorFoo>("decorated");
            sut.Register<IDecorWorker, DecorWorker>();

            IDecorWorker worker = sut.Resolve<IDecorWorker>();

            Assert.IsInstanceOf<FooDecorator>(worker.Foo);

            var decorator = worker.Foo as FooDecorator;

            Assert.IsInstanceOf<DecorFoo>(decorator.Foo);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Componentes_with_2_Decorators___Resolve_returns_correct_Types()
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IDecorFoo, DecorFoo>("decorated");
            sut.Register<IDecorFoo, FooDecorator>("decorated1");
            sut.Register<IDecorFoo, FooDecorator1>();
            sut.Register<IDecorWorker, DecorWorker>();

            IDecorWorker worker = sut.Resolve<IDecorWorker>();

            IDecorFoo foo = worker.Foo;
            Assert.IsInstanceOf<FooDecorator1>(foo);

            foo = (foo as FooDecorator1).Foo;
            Assert.IsInstanceOf<FooDecorator>(foo);

            foo = (foo as FooDecorator).Foo;
            Assert.IsInstanceOf<DecorFoo>(foo);
        }
    }
}