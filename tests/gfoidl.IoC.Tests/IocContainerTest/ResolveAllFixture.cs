﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class ResolveAllFixture
    {
        [Test]
        public void ResolveAll_StandardCtorAndOnlyOneContract_ReturnsCorrectCount()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("sub");

            int actual = sut.ResolveAll<IMath>().Count();

            Assert.AreEqual(2, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_StandardCtorAndOnlyOneContract_ReturnsCorrectInstances()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("sub");

            List<IMath> actual = sut.ResolveAll<IMath>().ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(IMath));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_Generics_ReturnsCorrectCount()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IList<>), typeof(List<>), "l1");
            sut.Register(typeof(IList<>), typeof(List<>), "l2");

            int actual = sut.ResolveAll<IList<int>>().Count();

            Assert.AreEqual(2, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_Generics_ReturnsCorrectInstances()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register(typeof(IList<>), typeof(List<>), "l1");
            sut.Register(typeof(IList<>), typeof(List<>), "l2");

            List<IList<int>> actual = sut.ResolveAll<IList<int>>().ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(IList<int>));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_WithSingletonInstance_SingletonIsUsed()
        {
            Add add = new Add();
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath>(add);
            sut.Register<IMath, Add>("add");

            List<IMath> actual = sut.ResolveAll<IMath>().ToList();

            Assert.AreEqual(2, actual.Count());

            int countSame      = 0;
            int countDifferent = 0;
            
            foreach (IMath math in actual)
                if (object.ReferenceEquals(add, math)) countSame++;
                else countDifferent++;

            Assert.AreEqual(countDifferent, 1);
            Assert.AreEqual(countSame     , 1);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_StandardCtorAndMultipleContracts_ReturnsCorrectCount()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("sub");
            sut.Register<IDisposable, MemoryStream>();

            int actual = sut.ResolveAll<IMath>().Count();

            Assert.AreEqual(2, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_StandardCtorAndMultipleContracts_ReturnsCorrectInstances()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add");
            sut.Register<IMath, Subtract>("sub");
            sut.Register<IDisposable, MemoryStream>();

            List<IMath> actual = sut.ResolveAll<IMath>().ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(IMath));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_StandardCtorAndNamedComponents_ReturnsCorrectCount()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add1");
            sut.Register<IMath, Add>("add2");
            sut.Register<IDisposable, MemoryStream>();

            int actual = sut.ResolveAll<IMath>().Count();

            Assert.AreEqual(2, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_StandardCtorAndNamedComponents_ReturnsCorrectInstances()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMath, Add>("add1");
            sut.Register<IMath, Add>("add2");
            sut.Register<IDisposable, MemoryStream>();

            List<IMath> actual = sut.ResolveAll<IMath>().ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(IMath));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgsAndOnlyOneContract_ReturnsCorrectCount()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMaster, Master>("master");
            sut.Register<IMaster, Mistress>("mistress");

            object slave = new object();

            int actual = sut.ResolveAll<IMaster>(slave).Count();

            Assert.AreEqual(2, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgsAndOnlyOneContract_ReturnsCorrectInstances()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMaster, Master>("master");
            sut.Register<IMaster, Mistress>("mistress");

            object slave = new object();

            List<IMaster> actual = sut.ResolveAll<IMaster>(slave).ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(IMaster));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgsAndOnlyOneContract_ReturnsCorrectObject()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMaster, Master>("master");
            sut.Register<IMaster, Mistress>("mistress");

            object slave = new object();

            List<IMaster> actual = sut.ResolveAll<IMaster>(slave).ToList();

            foreach (IMaster master in actual)
                Assert.AreSame(slave, master.Slave);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgsAndMultipleContracts_ReturnsCorrectCount()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMaster, Master>("master");
            sut.Register<IMaster, Mistress>("mistress");
            sut.Register<IDisposable, MemoryStream>();

            object slave = new object();

            int actual = sut.ResolveAll<IMaster>(slave).Count();

            Assert.AreEqual(2, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgsAndMultipleContracts_ReturnsCorrectInstances()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMaster, Master>("master");
            sut.Register<IMaster, Mistress>("mistress");
            sut.Register<IDisposable, MemoryStream>();

            object slave = new object();

            List<IMaster> actual = sut.ResolveAll<IMaster>(slave).ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(IMaster));
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgsAndMultipleContracts_ReturnsCorrectObject()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IMaster, Master>("master");
            sut.Register<IMaster, Mistress>("mistress");
            sut.Register<IDisposable, MemoryStream>();

            object slave = new object();

            List<IMaster> actual = sut.ResolveAll<IMaster>(slave).ToList();

            foreach (IMaster master in actual)
                Assert.AreSame(slave, master.Slave);
        }
        //---------------------------------------------------------------------
        [Test]
        public void ResolveAll_CtorWithArgThatIsResolved_ReturnsCorrectObject()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IFileSystemAdapter, FileSystemAdapter>();
            sut.Register<IBuildDirectoryStructureService, BuildDirectoryStructureService>();

            List<IBuildDirectoryStructureService> actual = sut
                .ResolveAll<IBuildDirectoryStructureService>()
                .ToList();

            CollectionAssert.AllItemsAreInstancesOfType(actual, typeof(BuildDirectoryStructureService));
            Assert.IsInstanceOf<FileSystemAdapter>(actual[0].FileSystemAdapter);
        }
    }
}