﻿using System;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class XmlFileFixture
    {
        private const string XmlFile = "containers.xml";
        //---------------------------------------------------------------------
        #region Register
        [Test]
        public void Register___correct_contract_count()
        {
            var sut = new IoCContainer();

            sut.RegisterFromXmlFile(XmlFile);

            Assert.AreEqual(2, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Register_from_xml_and_code___all_types_are_registered()
        {
            var sut = new IoCContainer();

            sut.RegisterFromXmlFile(XmlFile);
            int configTypes = sut.RegisteredContractCount;

            sut.Register<IFoo<int>, Foo<int>>();

            Assert.AreEqual(configTypes + 1, sut.RegisteredContractCount);
        }
        #endregion
        //---------------------------------------------------------------------
        #region Resolve
        [Test]
        public void Resolve___OK()
        {
            var sut = new IoCContainer();

            sut.RegisterFromXmlFile(XmlFile);

            var add = sut.Resolve<IMath>("add");
            var subtract = sut.Resolve<IMath>("subtract");

            Assert.IsInstanceOf<Add>(add);
            Assert.IsInstanceOf<Subtract>(subtract);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_generic_and_named___OK()
        {
            var sut = new IoCContainer();
            sut.RegisterFromXmlFile(XmlFile);

            var a = sut.Resolve<IGeneric<int>>("a");
            var b = sut.Resolve<IGeneric<string>>("b");

            Assert.IsInstanceOf<GenericA<int>>(a);
            Assert.IsInstanceOf<GenericB<string>>(b);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_default___returns_add_component()
        {
            var sut = new IoCContainer();
            sut.RegisterFromXmlFile(XmlFile);

            var comp = sut.Resolve<IMath>();

            Assert.IsInstanceOf<Add>(comp);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_Singleton_add___returns_same_instance()
        {
            var sut = new IoCContainer();
            sut.RegisterFromXmlFile(XmlFile);

            var comp1 = sut.Resolve<IMath>("singletonAdd");
            var comp2 = sut.Resolve<IMath>("singletonAdd");

            Assert.AreSame(comp1, comp2);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_name_not_registered___throws_InvalidOperation()
        {
            var sut = new IoCContainer();
            sut.RegisterFromXmlFile(XmlFile);

            Assert.Throws<InvalidOperationException>(() => sut.Resolve<IMath>("123456"));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_name_not_registered_and_use_default_set___default_used()
        {
            var sut = new IoCContainer();
            sut.RegisterFromXmlFile(XmlFile);

            var comp = sut.Resolve<IMath>("123456", true);

            Assert.IsInstanceOf<Add>(comp);
        }
        #endregion
    }
}