﻿using System;
using System.IO;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
    [TestFixture]
    public class SingletonFixture
    {
        [Test]
        public void Register_TypesAsSingleton_OK(
            [Values(true, false)]bool isSingleton)
        {
            IoCContainer sut = new IoCContainer();

            sut.Register<IDisposable, MemoryStream>(isSingleton);

            Assert.AreEqual(1, sut.RegisteredContractCount);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_TypeIsRegisteredAsSingleton_SameInstanceIsReturend()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IDisposable, MemoryStream>(true);

            var comp1 = sut.Resolve<IDisposable>();
            var comp2 = sut.Resolve<IDisposable>();

            Assert.That(comp1, Is.SameAs(comp2));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_TypeWithCtorArgumentsRegisteredAsSingleton_SameInstanceIsReturned()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IFileSystemAdapter, FileSystemAdapter>();
            sut.Register<IBuildDirectoryStructureService, BuildDirectoryStructureService>(true);

            var service1 = sut.Resolve<IBuildDirectoryStructureService>();
            var adapter1 = (service1 as BuildDirectoryStructureService).FileSystemAdapter;
            var service2 = sut.Resolve<IBuildDirectoryStructureService>();
            var adapter2 = (service2 as BuildDirectoryStructureService).FileSystemAdapter;

            Assert.IsInstanceOf<BuildDirectoryStructureService>(service1);
            Assert.IsInstanceOf<FileSystemAdapter>(adapter1);
            Assert.IsInstanceOf<FileSystemAdapter>(adapter2);
            Assert.AreSame(service1, service2);
            Assert.AreSame(adapter1, adapter2);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Resolve_TypeWithCtorArgumentsRegisteredAllRegisteredAsSingleton_SameInstanceIsReturned()
        {
            IoCContainer sut = new IoCContainer();
            sut.Register<IFileSystemAdapter, FileSystemAdapter>(true);
            sut.Register<IBuildDirectoryStructureService, BuildDirectoryStructureService>(true);

            var service1 = sut.Resolve<IBuildDirectoryStructureService>();
            var adapter1 = (service1 as BuildDirectoryStructureService).FileSystemAdapter;
            var service2 = sut.Resolve<IBuildDirectoryStructureService>();
            var adapter2 = (service2 as BuildDirectoryStructureService).FileSystemAdapter;

            Assert.IsInstanceOf<BuildDirectoryStructureService>(service1);
            Assert.IsInstanceOf<FileSystemAdapter>(adapter1);
            Assert.AreSame(service1, service2);
            Assert.AreSame(adapter1, adapter2);
        }
    }
}