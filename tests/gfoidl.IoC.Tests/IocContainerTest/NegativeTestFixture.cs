﻿using System;
using System.IO;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
	[TestFixture]
	public class NegativeTestFixture
	{
		#region Register
		[Test]
		[TestCase(null, typeof(MemoryStream))]
		[TestCase(typeof(IDisposable), null)]
		public void Register_TypesAreNull_ThrowsArgumentNull(Type contractType, Type implementationType)
		{
			IoCContainer sut = new IoCContainer();

			Assert.Throws<ArgumentNullException>(() => sut.Register(contractType, implementationType));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_InstanceIsNull_ThrowsArgumentNull()
		{
			IoCContainer sut = new IoCContainer();

			Assert.Throws<ArgumentNullException>(() => sut.Register<IDisposable>(null));
		}
		#endregion
		//---------------------------------------------------------------------
		#region Resolve
		[Test]
		public void Resolve_NoPublicCtor_ThrowsInvalidOperation()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable, MyDisposable>();

			Assert.Throws<InvalidOperationException>(() => sut.Resolve<IDisposable>());
		}
		//---------------------------------------------------------------------
		private class MyDisposable : IDisposable
		{
			internal MyDisposable() { }
			//-----------------------------------------------------------------
			public void Dispose() { }
		}
		#endregion
	}
}