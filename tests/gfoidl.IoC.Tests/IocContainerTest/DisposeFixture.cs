﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace gfoidl.IoC.Tests.IocContainerTest
{
	[TestFixture]
	public class DisposeFixture
	{
		[Test]
		public void Register_StandardIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Register<IDisposable, MemoryStream>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_StandardIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable, MemoryStream>();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Resolve<IDisposable>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_SingletonIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Register<IDisposable, MemoryStream>(true));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_SingletonIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable, MemoryStream>(true);
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Resolve<IDisposable>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_ObjectIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Register<IDisposable>(new MemoryStream()));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_ObjectIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IDisposable>(new MemoryStream());
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Resolve<IDisposable>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_NamedIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Register<IDisposable, MemoryStream>("abcd"));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_NamedIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register<IMath, Add>("add");
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Resolve<IMath>());
		}
		//---------------------------------------------------------------------
		[Test]
		public void Register_GenericIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Register(typeof(IList<>), typeof(List<>)));
		}
		//---------------------------------------------------------------------
		[Test]
		public void Resolve_GenericIsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Register(typeof(IList<>), typeof(List<>));
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.Resolve<IList<int>>());
		}
		//---------------------------------------------------------------------
#if NET_FULL
		[Test]
		public void RegisterFromConfiguration_IsDisposed_ThrowsObjectDisposed()
		{
			IoCContainer sut = new IoCContainer();
			sut.Dispose();

			Assert.Throws<ObjectDisposedException>(() => sut.RegisterFromConfiguration());
		}
#endif
		//---------------------------------------------------------------------
		[Test]
		public void Dispose_DisposableImplicitRegistered_WhenSingletonComponentIsDisposed()
		{
			Assert.Throws<ObjectDisposedException>(() =>
			{
				IoCContainer sut = new IoCContainer();
				sut.Register<IDisposable, DisposableImplict>(true);

				MemoryStream ms = (sut.Resolve<IDisposable>() as DisposableImplict)?.MemoryStream;

				sut.Dispose();
				ms.Seek(0, SeekOrigin.Begin);
			});
		}
		//---------------------------------------------------------------------
		[Test]
		public void Dispose_DisposableExplicitRegistered_WhenSingletonComponentIsDisposed()
		{
			Assert.Throws<ObjectDisposedException>(() =>
			{
				IoCContainer sut = new IoCContainer();
				sut.Register<IDisposable, DisposableExplicit>(true);

				MemoryStream ms = (sut.Resolve<IDisposable>() as DisposableExplicit)?.MemoryStream;

				sut.Dispose();
				ms.Seek(0, SeekOrigin.Begin);
			});
		}
	}
}