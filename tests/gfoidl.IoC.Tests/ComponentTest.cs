﻿using System;
using gfoidl.IoC.Internal;
using NUnit.Framework;

namespace gfoidl.IoC.Tests
{
    [TestFixture]
    public class ComponentTest
    {
        #region Ctor
        [Test]
        public void Ctor_NameIsNullOrWhitespace_EmptyStringIsSet([Values(null, "", " ", "\t")]string name)
        {
            Component sut = new Component(null, name, false);

            Assert.AreEqual(string.Empty, sut.Name);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Ctor_ArgsGiven_PropertiesSetCorrectly([Values(true, false)]bool isSingleton)
        {
            Component sut = new Component(typeof(Foo), "foo", isSingleton);

            Assert.AreEqual(typeof(Foo), sut.Type);
            Assert.AreEqual("foo", sut.Name);
            Assert.AreEqual(isSingleton, sut.IsSingleton);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Ctor_SingletonObjectGiven_CorrectTypeSet()
        {
            Component sut = new Component("foo", new Foo());

            Assert.AreEqual(typeof(Foo), sut.Type);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Ctor_SingletonObjectGiven_IsSignletonIsTrue()
        {
            Component sut = new Component("foo", new Foo());

            Assert.IsTrue(sut.IsSingleton);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Ctor_SingletonObjectGiven_CorrectInstanceSet()
        {
            Foo foo = new Foo();
            Component sut = new Component("foo", foo);

            Assert.AreSame(foo, sut.SingletonObject);
        }
        #endregion
        //---------------------------------------------------------------------
        #region IDisposable
        [Test]
        public void Dispose_DisposableImplicitAsType_IsDisposed()
        {
            DisposableImplict comp = new DisposableImplict();
            Component sut = new Component(null, comp);

            sut.Dispose();

            Assert.Throws<ObjectDisposedException>(() => comp.MemoryStream.Seek(0, System.IO.SeekOrigin.Begin));
        }
        //---------------------------------------------------------------------
        [Test]
        public void Dispose_DisposableExplicitAsType_IsDisposed()
        {
            DisposableExplicit comp = new DisposableExplicit();
            Component sut = new Component(null, comp);

            sut.Dispose();

            Assert.Throws<ObjectDisposedException>(() => comp.MemoryStream.Seek(0, System.IO.SeekOrigin.Begin));
        }
        #endregion
        //---------------------------------------------------------------------
        #region CreateImplementation
        /* Wird über den IoCContainer getestet, da die Test von der Version vor
         * der Optimierung übernommen werden können.
         */
        #endregion
    }
}