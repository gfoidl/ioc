﻿using System;

namespace gfoidl.IoC.Tests
{
    #region BuildDirectoryStructureService
    public interface IFileSystemAdapter { }
    //-------------------------------------------------------------------------
    public class FileSystemAdapter : IFileSystemAdapter { }
    //-------------------------------------------------------------------------
    public interface IBuildDirectoryStructureService
    {
        IFileSystemAdapter FileSystemAdapter { get; }
    }
    //-------------------------------------------------------------------------
    public class BuildDirectoryStructureService : IBuildDirectoryStructureService
    {
        public IFileSystemAdapter FileSystemAdapter { get; private set; }
        //---------------------------------------------------------------------
        public BuildDirectoryStructureService(IFileSystemAdapter fileSystemAdapter)
        {
            this.FileSystemAdapter = fileSystemAdapter;
        }
    }
    #endregion
    //-------------------------------------------------------------------------
    #region Disposable
    public class Foo : IDisposable
    {
        public void Dispose() { }
    }
    #endregion
    //-------------------------------------------------------------------------
    #region Math
    public interface IMath { }
    public class Add : IMath { }
    public class Subtract : IMath { }
    #endregion
    //-------------------------------------------------------------------------
    #region Arguments
    public interface IMaster
    {
        string Name  {get;}
        object Slave { get; }
    }
    public class Master : IMaster
    {
        public string Name { get; set; }
        public object Slave { get; set; }

        public Master(object slave)
        {
            this.Slave = slave;
        }
    }
    public class Mistress : IMaster
    {
        public string Name { get; set; }
        public object Slave { get; set; }

        public Mistress(object slave)
        {
            this.Slave = slave;
        }
    }
    public class Emperor : IMaster
    {
        public string Name { get; set; }
        public object Slave { get; set; }

        public Emperor(string name)
        {
            this.Name = name;
        }
    }
    #endregion
    //-------------------------------------------------------------------------
    #region Optional Arguments
    public interface IFuzyGuzy { }

    public class FuzyGuzy : IFuzyGuzy
    {
        public object Arg { get; private set; }
        public FuzyGuzy(object arg = null)
        {
            this.Arg = arg;
        }
    }

    public class FuzyWuzy : IFuzyGuzy
    {
        public string Arg { get; private set; }
        public FuzyWuzy(string arg = "yeaaah")
        {
            this.Arg = arg;
        }
    }
    #endregion
    //-------------------------------------------------------------------------
    #region NonInterface Types
    public class Parent { }
    public class Child1 : Parent { }
    public class Child2 : Parent { }
    #endregion
    //-------------------------------------------------------------------------
    #region Sub-Interfaces
    public interface IParent { }
    public interface IChild : IParent { }
    public class ParentClass : IParent { }
    public class ChildClass : ParentClass, IChild { }
    #endregion
    //-------------------------------------------------------------------------
    #region Attributes
    public interface IMyFoo { }

    [Component(typeof(IMyFoo))]
    public class MyFoo : IMyFoo { }
    //-------------------------------------------------------------------------
    public interface IMyMath { }

    [Component(typeof(IMyMath), "add")]
    public class MyAdd : IMyMath { }

    [Component(typeof(IMyMath), "subtract")]
    public class MySubtract : IMyMath { }

    [Component(typeof(IMyMath), "divide")]
    internal class MyDivide : IMyMath { }
    //-------------------------------------------------------------------------
    public interface IMyGeneric<T> { }

    [Component(typeof(IMyGeneric<>))]
    public class MyGeneric<T> : IMyGeneric<T> { }

    internal interface IMyInternalInterface { }

    [Component(typeof(IMyInternalInterface), "public")]
    public class MyPublicClass : IMyInternalInterface { }

    [Component(typeof(IMyInternalInterface), "internal")]
    internal class MyInternalClass : IMyInternalInterface { }
    //-------------------------------------------------------------------------
    [Component(typeof(IParent))]
    [Component(typeof(IMath))]
    public class MyClassWithMultipleContracts : IParent, IMath { }
    #endregion
    //-------------------------------------------------------------------------
    #region Decorator
    public interface IDecorFoo
    {
        void DoIt();
    }

    public class DecorFoo : IDecorFoo
    {
        public void DoIt() { }
    }

    public class FooDecorator : IDecorFoo
    {
        public IDecorFoo Foo { get; private set; }

        public FooDecorator(IDecorFoo foo)
        {
            this.Foo = foo;
        }

        public void DoIt() { }
    }

    public class FooDecorator1 : IDecorFoo
    {
        public IDecorFoo Foo { get; private set; }

        public FooDecorator1(IDecorFoo foo)
        {
            this.Foo = foo;
        }

        public void DoIt() { }
    }

    public interface IDecorWorker
    {
        IDecorFoo Foo { get; }
    }

    public class DecorWorker : IDecorWorker
    {
        public IDecorFoo Foo { get; private set; }

        public DecorWorker(IDecorFoo foo)
        {
            this.Foo = foo;
        }
    }
    #endregion
}