﻿namespace gfoidl.IoC.Tests
{
    public interface IGeneric<T> { }
    public class GenericA<T> : IGeneric<T> { }
    public class GenericB<T> : IGeneric<T> { }
    //-------------------------------------------------------------------------
    public interface IFoo<T> { }
    public class Foo<T> : IFoo<T>
    {
        public IGeneric<T> Component { get; private set; }
        //---------------------------------------------------------------------
        public Foo(IGeneric<T> component)
        {
            this.Component = component;
        }
    }
    //---------------------------------------------------------------------
    public class ModelA { }
    public class ModelB { }
    public class FooA : IFoo<ModelA> { }
    public class FooB : IFoo<ModelB> { }
}