﻿using System;
using System.IO;

namespace gfoidl.IoC.Tests
{
    public class DisposableImplict : IDisposable
    {
        public MemoryStream MemoryStream = new MemoryStream();
        //---------------------------------------------------------------------
        public void Dispose()
        {
            this.MemoryStream.Dispose();
        }
    }
    //-------------------------------------------------------------------------
    public class DisposableExplicit : IDisposable
    {
        public MemoryStream MemoryStream = new MemoryStream();
        //---------------------------------------------------------------------
        void IDisposable.Dispose()
        {
            this.MemoryStream.Dispose();
        }
    }
}