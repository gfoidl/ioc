﻿using System;

namespace gfoidl.IoC.PerfTests
{
    public interface IFoo
    {
        void Do();
    }
    //-------------------------------------------------------------------------
    public interface IBar
    {
        int Get();
    }
    //-------------------------------------------------------------------------
    public class Foo : IFoo
    {
        private readonly IBar _bar;
        //---------------------------------------------------------------------
        public Foo(IBar bar)
        {
            _bar = bar ?? throw new ArgumentNullException(nameof(bar));
        }
        //---------------------------------------------------------------------
        public void Do() => Console.WriteLine(_bar.Get());
    }
    //-------------------------------------------------------------------------
    public class Bar : IBar
    {
        public int Get() => 42;
    }
}