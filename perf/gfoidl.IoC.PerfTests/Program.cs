﻿#define COMPARE
//-----------------------------------------------------------------------------
using System;
using System.Diagnostics;
using Microsoft.Extensions.DependencyInjection;

namespace gfoidl.IoC.PerfTests
{
    static class Program
    {
        static void Main()
        {
            const int N = 5_000_000;
            var ioc = new IoCContainer();
            ioc.Register<IFoo, Foo>();
            ioc.Register<IBar, Bar>();

            IFoo foo = ioc.Resolve<IFoo>();
            foo.Do();
#if COMPARE
            IServiceProvider services = new ServiceCollection()
                .AddTransient<IFoo, Foo>()
                .AddTransient<IBar, Bar>()
                .BuildServiceProvider();

            IFoo fo1 = services.GetRequiredService<IFoo>();
            fo1.Do();
#endif
            Console.WriteLine($"Starting perf-test with {N} iterations");

            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < N; ++i)
                foo = ioc.Resolve<IFoo>();
            sw.Stop();
            Console.WriteLine($"Time [ms]: {sw.ElapsedMilliseconds}");
#if COMPARE
            sw.Restart();
            for (int i = 0; i < N; ++i)
                foo = services.GetRequiredService<IFoo>();
            sw.Stop();
            Console.WriteLine($"Time [ms]: {sw.ElapsedMilliseconds}");
#endif
        }
    }
}