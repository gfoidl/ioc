﻿using System;
using System.Diagnostics;

namespace gfoidl.IoC
{
    /// <summary>
    /// A base-class for <see cref="IDisposable"/>.
    /// </summary>
    [DebuggerNonUserCode]
    public abstract class DisposableObject : IDisposable
    {
        private readonly object _syncRoot = new object();
        //---------------------------------------------------------------------
        #region IDisposable Members
        private volatile bool _isDisposed = false;
        //---------------------------------------------------------------------
        /// <summary>
        /// Throws an <see cref="ObjectDisposedException"/> when the 
        /// <see cref="IoCContainer"/> is disposed.
        /// </summary>
        protected void ThrowIfDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(this.ToString());
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Disposes the <see cref="IoCContainer"/> (implementation).
        /// </summary>
        /// <param name="disposing">
        /// Indicates wheter this method is called from user code or form the GC.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
                lock (_syncRoot)
                    if (!_isDisposed)
                    {
                        if (disposing)
                            this.DisposeCore();

                        _isDisposed = true;
                    }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Disposes all managed Ressources.
        /// </summary>
        protected abstract void DisposeCore();
        //---------------------------------------------------------------------
        /// <summary>
        /// Releases all resources used by the current instance of the class.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}