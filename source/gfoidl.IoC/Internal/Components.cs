﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace gfoidl.IoC.Internal
{
#if !DEBUG
    [DebuggerNonUserCode]
#endif
    internal class Components : DisposableObject, IEnumerable<Component>
    {
        private readonly FastComponentDictionary _components;
        private readonly Type                    _contractType;
        //---------------------------------------------------------------------
        // Wird eigentlich nur zum Testen benötig.
        public int ComponentCount { get { return _components.Count; } }
        //---------------------------------------------------------------------
        public Components() => _components = new FastComponentDictionary();
        //---------------------------------------------------------------------
        public Components(Type contractType) : this()
        {
            _contractType = contractType;
        }
        //---------------------------------------------------------------------
        // Wird eigentlich nur zum Testen benötig.
        public bool ContainsName(string name)         => _components.ContainsKey(name ?? string.Empty);
        public IEnumerable<string> GetNames()         => _components.GetNames();
        public void AddComponent(Component component) => _components.AddComponent(component);
        public Component GetDefaultComponent()        => this.GetComponent(string.Empty, false);
        //---------------------------------------------------------------------
        public Component GetComponent(string name, bool useDefaultIfNamedNotFound, Type notThisType = null)
        {
            if (notThisType != null && _contractType.IsAssignableFrom(notThisType))
                return this.GetComponentExcludingType(notThisType);

            if (_components.TryGetValue(name ?? string.Empty, out Component component))
                return component;

            return GetComponentSlow();
            //-----------------------------------------------------------------
            Component GetComponentSlow()
            {
                if (useDefaultIfNamedNotFound)
                    return this.GetComponent(null, false);

                throw new InvalidOperationException(string.Format(
                    Resources.Strings.NamedComponentNotRegistered,
                    _contractType,
                    name ?? string.Empty));
            }
        }
        //---------------------------------------------------------------------
        private Component GetComponentExcludingType(Type notThisType)
        {
            foreach (var component in _components.Values().OrderByDescending(c => c.Name))
                if (component.Type != notThisType) return component;

            throw new InvalidOperationException(string.Format(
                Resources.Strings.TheOnlyTypeIsExcluded,
                _contractType,
                notThisType));
        }
        //---------------------------------------------------------------------
        #region IEnumerable<Component> Members
        public IEnumerator<Component> GetEnumerator()                                 => _components.Values().GetEnumerator();
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() => this.GetEnumerator();
        #endregion
        //---------------------------------------------------------------------
        #region IDisposable Members
        protected override void DisposeCore()
        {
            foreach (var cmp in _components.Values())
                cmp.Dispose();
        }
        #endregion
    }
}