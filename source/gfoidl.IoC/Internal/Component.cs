﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using gfoidl.IoC.Resources;

namespace gfoidl.IoC.Internal
{
#if !DEBUG
    [DebuggerNonUserCode]
#endif
    [DebuggerDisplay("Name: {Name}, Type: {Type.Name}, IsSingleton: {IsSingleton}")]
    internal class Component : DisposableObject
    {
        private delegate object CtorDelegate(params object[] args);
        //---------------------------------------------------------------------
        private ConstructorInfo _ctorInfo;
        private ParameterInfo[] _ctorParameters;
        private CtorDelegate    _ctorDelegate;
        //---------------------------------------------------------------------
        public Type Type              { get; }
        public string Name            { get; }
        public bool IsSingleton       { get; }
        public object SingletonObject { get; private set; }
        //---------------------------------------------------------------------
        public Component(Type type, string name, bool isSingleton)
        {
            this.Type        = type;
            this.Name        = string.IsNullOrWhiteSpace(name) ? string.Empty : name;
            this.IsSingleton = isSingleton;
        }
        //---------------------------------------------------------------------
        public Component(string name, object singletonObject) : this(singletonObject.GetType(), name, true)
        {
            this.SingletonObject = singletonObject;
        }
        //---------------------------------------------------------------------
        public object CreateImplementation(Func<Type, object> resolve, params object[] args)
        {
            if (this.SingletonObject != null) return this.SingletonObject;

            object instance = this.CreateImplementationCore(resolve, args);

            if (this.IsSingleton) this.SingletonObject = instance;

            return instance;
        }
        //---------------------------------------------------------------------
        private object CreateImplementationCore(Func<Type, object> resolve, params object[] args)
        {
            this.EnsureCtorInfoInitialized();
            this.EnsureCtorDelegateInitialized();

            if (_ctorParameters.Length == 0)
                return _ctorDelegate();

            if (args == null || args.Length == 0)
                this.GetArguments(resolve, _ctorParameters, ref args);

            return _ctorDelegate(args);
        }
        //---------------------------------------------------------------------
        private void GetArguments(Func<Type, object> resolve, ParameterInfo[] ctorParameters, ref object[] args)
        {
            try
            {
                args = new object[_ctorParameters.Length];

                for (int i = 0; i < ctorParameters.Length; ++i)
                {
                    if (ctorParameters[i].HasDefaultValue)
                    {
                        args[i] = ctorParameters[i].DefaultValue;
                        continue;
                    }

                    if (ctorParameters[i].ParameterType == typeof(object))
                        throw new TargetParameterCountException();

                    args[i] = resolve(ctorParameters[i].ParameterType);
                }
            }
            catch (InvalidOperationException iopex)
            {
                throw new Exception(
                    string.Format(Strings.ResolveInvalidOperationException, this.Type, iopex.Message),
                    iopex);
            }
        }
        //---------------------------------------------------------------------
        private void EnsureCtorInfoInitialized()
        {
            if (_ctorInfo == null) this.GetConstructorInfo();
        }
        //---------------------------------------------------------------------
        private void EnsureCtorDelegateInitialized()
        {
            if (_ctorDelegate == null) this.CreateCtorDelegate();
        }
        //---------------------------------------------------------------------
        private void GetConstructorInfo()
        {
            try
            {
                _ctorInfo       = this.Type.GetConstructors()[0];
                _ctorParameters = _ctorInfo.GetParameters();
            }
            catch (IndexOutOfRangeException)
            {
                string msg = string.Format(
                    CultureInfo.CurrentCulture,
                    Resources.Strings.ConstructorInfoIndexOutOfRange,
                    this.Type);
                throw new InvalidOperationException(msg);
            }
        }
        //---------------------------------------------------------------------
        private void CreateCtorDelegate()
        {
            var dm            = new DynamicMethod("Ctor", typeof(object), new Type[] { typeof(object[]) }, this.Type);
            ILGenerator ilGen = dm.GetILGenerator();

            if (_ctorParameters.Length > 0)
            {
                Label argsOK = ilGen.DefineLabel();
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldlen);
                ilGen.Emit(OpCodes.Ldc_I4, _ctorParameters.Length);
                ilGen.Emit(OpCodes.Beq, argsOK);

                ilGen.Emit(OpCodes.Newobj, typeof(TargetParameterCountException).GetConstructor(Type.EmptyTypes));
                ilGen.Emit(OpCodes.Throw);

                ilGen.MarkLabel(argsOK);

                for (int i = 0; i < _ctorParameters.Length; ++i)
                {
                    ilGen.Emit(OpCodes.Ldarg_0);
                    ilGen.Emit(OpCodes.Ldc_I4, i);
                    ilGen.Emit(OpCodes.Ldelem_Ref);

                    if (_ctorParameters[i].ParameterType.IsValueType)
                        ilGen.Emit(OpCodes.Unbox_Any, _ctorParameters[i].ParameterType);
                }
            }

            ilGen.Emit(OpCodes.Newobj, _ctorInfo);
            ilGen.Emit(OpCodes.Ret);

            _ctorDelegate = dm.CreateDelegate(typeof(CtorDelegate)) as CtorDelegate;
        }
        //---------------------------------------------------------------------
        protected override void DisposeCore()
        {
            IDisposable disposable = this.SingletonObject as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
    }
}