﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace gfoidl.IoC.Internal
{
    internal class FastComponentDictionary
    {
        private int                                     _count;
        private string                                  _firstName;
        private Component                               _firstComponent;
        private ConcurrentDictionary<string, Component> _components;
        //---------------------------------------------------------------------
        public int Count => _count;
        //---------------------------------------------------------------------
        public void AddComponent(Component component)
        {
            switch (_count)
            {
                case 0:
                    _firstName      = component.Name;
                    _firstComponent = component;
                    _count++;
                    break;
                case 1:
                    _components = new ConcurrentDictionary<string, Component>
                    {
                        [_firstComponent.Name] = _firstComponent,
                        [component.Name]       = component
                    };
                    _count = 2;
                    break;
                default:
                    _components.TryAdd(component.Name, component);
                    break;
            }
        }
        //---------------------------------------------------------------------
        public bool TryGetValue(string name, out Component component)
        {
            if (_count != 1) return TryGetValueSlow(out component);

            component = _firstComponent;
            return name == _firstName;
            //-----------------------------------------------------------------
            bool TryGetValueSlow(out Component cmp)
            {
                if (_count == 0)
                {
                    cmp = null;
                    return false;
                }

                return _components.TryGetValue(name, out cmp);
            }
        }
        //---------------------------------------------------------------------
        public bool ContainsKey(string name)
        {
            switch (_count)
            {
                case 0 : return false;
                case 1 : return _firstName == name;
                default: return _components.ContainsKey(name);
            }
        }
        //---------------------------------------------------------------------
        public IEnumerable<string> GetNames()
        {
            switch (_count)
            {
                case 0 : return new string[0];
                case 1 : return YieldFirst();
                default:
                    return _components.Keys;
            }
            //-----------------------------------------------------------------
            IEnumerable<string> YieldFirst()
            {
                yield return _firstName;
            }
        }
        //---------------------------------------------------------------------
        public IEnumerable<Component> Values()
        {
            switch (_count)
            {
                case 0 : return new Component[0];
                case 1 : return YieldFirst();
                default:
                    return _components.Values;
            }
            //-----------------------------------------------------------------
            IEnumerable<Component> YieldFirst()
            {
                yield return _firstComponent;
            }
        }
    }
}