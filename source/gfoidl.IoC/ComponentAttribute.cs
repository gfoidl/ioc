﻿using System;
using System.Reflection;

namespace gfoidl.IoC
{
    /// <summary>
    /// Defines an attribute that can be used in conjunction with 
    /// <see cref="IoCContainer.RegisterFromAttributes" />.
    /// </summary>
    /// <seealso cref="IoCContainer.RegisterFromAttributes" />
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class ComponentAttribute : Attribute
    {
        private readonly Type   _contractType;
        private readonly string _name;
        //---------------------------------------------------------------------
        /// <summary>
        /// Instantiates a new <see cref="ComponentAttribute" />.
        /// </summary>
        /// <param name="contractType">
        /// The type of the contract, to which this component should be registered.
        /// </param>
        /// <param name="name">
        /// The optional name of the component.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="contractType" /> is <c>null</c>.
        /// </exception>
        public ComponentAttribute(Type contractType, string name = null)
        {
            _contractType = contractType ?? throw new ArgumentNullException(nameof(contractType));
            _name = name;
        }
        //---------------------------------------------------------------------
        internal static void Register(IoCContainer ioc, params Assembly[] assembliesToCheckForComponents)
        {
            foreach (Assembly assembly in assembliesToCheckForComponents)
                foreach (Type type in assembly.GetTypes())
                {
                    if (!type.IsClass) continue;

                    foreach (ComponentAttribute attr in type.GetCustomAttributes<ComponentAttribute>(false))
                        ioc.Register(attr._contractType, type, attr._name);
                }
        }
    }
}