﻿using System;
using System.Configuration;

namespace gfoidl.IoC.Configuration
{
    /// <summary>
    /// Collection of configuration elements of the container.
    /// </summary>
    public sealed class ContainersConfigurationElementCollection : BaseConfigurationElementCollection<ContainerConfigurationElement>
    {
        /// <summary>
        /// Returns the configured component.
        /// </summary>
        /// <param name="element">The configuration element.</param>
        /// <returns>The configured container.</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null)
                throw new ArgumentNullException("element");
            //-----------------------------------------------------------------
            return (element as ContainerConfigurationElement).InterfaceType;
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Returns the container.
        /// </summary>
        /// <param name="type">
        /// The type from whom the container shall be returned.
        /// </param>
        /// <returns>The container.</returns>
        public ContainerConfigurationElement GetContainer(Type type)
        {
            if (type == null) throw new ArgumentNullException(nameof(type));

            foreach (ContainerConfigurationElement container in this)
            {
                Type interfaceType = Type.GetType(container.InterfaceType);
                if (interfaceType != null)
                    if (type == interfaceType)
                        return container;
            }

            return null;
        }
    }
}