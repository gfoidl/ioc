﻿using System.Configuration;

namespace gfoidl.IoC.Configuration
{
    /// <summary>
    /// A configuration element in the app.config.
    /// </summary>
    /// <typeparam name="T">The type of the configuration element.</typeparam>
    public abstract class BaseConfigurationElementCollection<T> : ConfigurationElementCollection
        where T : ConfigurationElement, new()
    {
        /// <summary>
        /// The type of the configuration element collection.
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Creates per factory a configuration element.
        /// </summary>
        /// <returns>The configuration element.</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new T();
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Strongly typed indexer for the configuration elements.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The configuration element for the given key.</returns>
        public T this[object key]
        {
            get { return this.BaseGet(key) as T; }
        }
    }
}