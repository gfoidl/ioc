﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;

namespace gfoidl.IoC.Configuration
{
    /// <summary>
    /// The configuration element for a component.
    /// </summary>
    public sealed class ComponentConfigurationElement : ConfigurationElement
    {
        /// <summary>
        /// Implementatio of the component.
        /// </summary>
        [ConfigurationProperty("componentType", IsRequired = true)]
        public string ComponentType
        {
            get { return (string)this["componentType"]; }
            set { this["componentType"] = value; }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Component's name.
        /// </summary>
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Determines whether the component should be a singleton or not.
        /// </summary>
        [ConfigurationProperty("singleton", IsRequired = false)]
        public bool IsSingleton
        {
            get { return (bool)this["singleton"]; }
            set { this["singleton"] = value; }
        }
        /*
        //---------------------------------------------------------------------
        /// <summary>
        /// Argument for the constructor of the concrete type.
        /// </summary>
        /// <remarks>
        /// The name matches the type of the argument and the value is the
        /// value ;-)<br />
        /// The value has to have a <see cref="TypeConverter"/>.
        /// </remarks>
        /// <example>
        /// The following example shows how an argument <c>int = 5</c> is set:
        /// <code>
        /// <arguments>
        ///     <add name="System.Int32" value="5" />
        /// </arguments>
        /// </code>
        /// </example>
        [ConfigurationProperty("arguments", IsRequired = false)]
        public NameValueConfigurationCollection Settings
        {
            get { return (NameValueConfigurationCollection)this["arguments"]; }
            set { this["arguments"] = value; }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Returns the arguments of the constructor.
        /// </summary>
        /// <returns>The ctor's arguments.</returns>
        internal object[] GetArguments()
        {
            if (this.Settings.Count == 0) return null;

            List<object> arguments = new List<object>();
            foreach (NameValueConfigurationElement nvc in this.Settings)
            {
                Type t = Type.GetType(nvc.Name);
                TypeConverter converter = TypeDescriptor.GetConverter(t);
                object argument = converter.ConvertFromString(nvc.Value);
                arguments.Add(argument);
            }

            return arguments.ToArray();
        }
        */
    }
}