﻿using System.Configuration;

namespace gfoidl.IoC.Configuration
{
    /// <summary>
    /// The configuration-section for the IoC-Container.
    /// </summary>
    public sealed class IoCConfigurationSection : ConfigurationSection
    {
        /// <summary>
        /// Collection of the configuration elements.
        /// </summary>
        [ConfigurationProperty("containers")]
        [ConfigurationCollection(typeof(ContainersConfigurationElementCollection), AddItemName = "container")]
        public ContainersConfigurationElementCollection Containers
        {
            get { return (ContainersConfigurationElementCollection)this["containers"]; }
            //set { this["containers"] = value; }
        }
    }
}