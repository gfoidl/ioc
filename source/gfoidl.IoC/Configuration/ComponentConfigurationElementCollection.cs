﻿using System;
using System.Configuration;

namespace gfoidl.IoC.Configuration
{
    /// <summary>
    /// The configuration element collection.
    /// </summary>
    public sealed class ComponentConfigurationElementCollection
        : BaseConfigurationElementCollection<ComponentConfigurationElement>
    {
        /// <summary>
        /// Returns the configured component.
        /// </summary>
        /// <param name="element">The configuration element.</param>
        /// <returns>Configured component.</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            if (element == null) throw new ArgumentNullException("element");

            return (element as ComponentConfigurationElement).Name;
        }
    }
}