﻿using System.Configuration;

namespace gfoidl.IoC.Configuration
{
    /// <summary>
    /// Provides the configuration element for the container.
    /// </summary>
    public sealed class ContainerConfigurationElement : ConfigurationElement
    {
        /// <summary>
        /// The interface type.
        /// </summary>
        [ConfigurationProperty("interfaceType")]
        public string InterfaceType
        {
            get { return (string)this["interfaceType"]; }
            set { this["interfaceType"] = value; }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// The default component.
        /// </summary>
        [ConfigurationProperty("defaultComponent")]
        public string DefaultComponent
        {
            get { return (string)this["defaultComponent"]; }
            set { this["defaultComponent"] = value; }
        }
        //---------------------------------------------------------------------
        /// <summary>
        /// Collection of the component configuration elements.
        /// </summary>
        [ConfigurationProperty("components")]
        [ConfigurationCollection(typeof(ComponentConfigurationElement), AddItemName = "component")]
        public ComponentConfigurationElementCollection Components
        {
            get { return (ComponentConfigurationElementCollection)this["components"]; }
            //set { this["components"] = value; }
        }
    }
}