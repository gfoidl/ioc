﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Xml.Linq;
using gfoidl.IoC.Internal;

#if NET_FULL
using System.Configuration;
using gfoidl.IoC.Configuration;
#endif

namespace gfoidl.IoC
{
    /// <include
    /// file='IoCContainer.doc.xml' 
    /// path='doc/entry[@name="Class"]/*'/>
#if !DEBUG
    [DebuggerNonUserCode]
#endif
    public sealed class IoCContainer : DisposableObject
    {
#if DEBUG
        private static int _instanceCounter = 0;
#endif
        private readonly ConcurrentDictionary<Type, Components> _container;
        //---------------------------------------------------------------------
        private static IoCContainer _default;
        private static bool         _isDefaultInitialized = false;
        private static object       _syncLock = new object();
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Default"]/*'/>
        public static IoCContainer Default
        {
            get
            {
                return LazyInitializer.EnsureInitialized(ref _default, ref _isDefaultInitialized, ref _syncLock);
            }
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="RegisteredContractCount"]/*'/>
        public int RegisteredContractCount => _container.Count;
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Ctor"]/*'/>
        public IoCContainer()
        {
#if DEBUG
            Console.WriteLine(++_instanceCounter);
#endif
            _container = new ConcurrentDictionary<Type, Components>();
        }
        //---------------------------------------------------------------------
        #region Register
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register1"]/*'/>
        public void Register<TContract, TImplementation>()
        {
            this.Register<TContract, TImplementation>(false);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register2"]/*'/>
        public void Register<TContract, TImplementation>(bool isSingleton)
        {
            this.Register<TContract, TImplementation>(null, isSingleton);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register3"]/*'/>
        public void Register<TContract, TImplementation>(string name)
        {
            this.Register<TContract, TImplementation>(name, false);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register4"]/*'/>
        public void Register<TContract, TImplementation>(string name, bool isSingleton)
        {
            this.Register(typeof(TContract), typeof(TImplementation), name, isSingleton);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register5"]/*'/>
        public void Register(Type contractType, Type implementationType)
        {
            this.Register(contractType, implementationType, null, false);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register6"]/*'/>
        public void Register(Type contractType, Type implementationType, string name)
        {
            this.Register(contractType, implementationType, name, false);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register7"]/*'/>
        public void Register<TContract>(TContract instance)
        {
            this.Register(instance, null);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Register8"]/*'/>
        public void Register<TContract>(TContract instance, string name)
        {
#pragma warning disable RCS1165
            if (instance == null) throw new ArgumentNullException(nameof(instance));
#pragma warning restore RCS1165
            this.ThrowIfDisposed();

            Type contractType     = typeof(TContract);
            Components components = this.GetOrCreateComponents(contractType);
            Component component   = new Component(name, instance);

            components.AddComponent(component);
        }
        //---------------------------------------------------------------------
#if NET_FULL
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="RegisterConfig1"]/*'/>
        public void RegisterFromConfiguration()
        {
            this.RegisterFromConfiguration(ConfigurationManager.GetSection("gfoidl.IoC") as IoCConfigurationSection);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="RegisterConfig2"]/*'/>
        public void RegisterFromConfiguration(string sectionName)
        {
            // .net liefert auch sonst die korrekte Exception, Mono nicht -> daher expliziter Test auf null
            if (sectionName == null) throw new ArgumentNullException(nameof(sectionName));

            this.RegisterFromConfiguration(ConfigurationManager.GetSection(sectionName) as IoCConfigurationSection);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="RegisterConfig3"]/*'/>
        public void RegisterFromConfiguration(IoCConfigurationSection config)
        {
            if (config == null) throw new ArgumentNullException(nameof(config));

            this.ThrowIfDisposed();

            foreach (ContainerConfigurationElement container in config.Containers)
            {
                Type contractType = Type.GetType(container.InterfaceType);
                string defaultComponentName = container.DefaultComponent;

                foreach (ComponentConfigurationElement component in container.Components)
                {
                    Type implementationType = Type.GetType(component.ComponentType);
                    this.Register(contractType, implementationType, component.Name, component.IsSingleton);

                    if (component.Name == defaultComponentName)
                        this.Register(contractType, implementationType, null, component.IsSingleton);
                }
            }
        }
#endif
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="RegisterFromAttributes"]/*'/>
        public void RegisterFromAttributes(params Assembly[] assembliesToCheckForComponents)
        {
            if (assembliesToCheckForComponents == null || assembliesToCheckForComponents.Length == 0)
            {
                var assemblies = new HashSet<Assembly>(AppDomain.CurrentDomain.GetAssemblies());

                foreach (Assembly asm in assemblies.ToArray())
                    try
                    {
                        assemblies.UnionWith(asm.GetReferencedAssemblies().Select(an => Assembly.Load(an)));
                    }
                    catch { }

                assembliesToCheckForComponents = assemblies.ToArray();
            }

            ComponentAttribute.Register(this, assembliesToCheckForComponents);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="RegisterFromXmlFile"]/*'/>
        public void RegisterFromXmlFile(string fileName)
        {
            XElement config     = XElement.Load(fileName);
            XElement containers = config.Element("containers");

            foreach (XElement container in containers.Elements("container"))
            {
                Type contractType           = Type.GetType(container.Attribute("interfaceType").Value);
                string defaultComponentName = (string)container.Attribute("defaultComponent");

                XElement compomonents = container.Element("components");

                foreach (XElement component in compomonents.Elements("component"))
                {
                    Type implementationType = Type.GetType(component.Attribute("componentType").Value);
                    string componentName    = component.Attribute("name").Value;
                    bool.TryParse((string)component.Attribute("singleton"), out bool isSingleton);

                    this.Register(contractType, implementationType, componentName, isSingleton);

                    if (componentName == defaultComponentName)
                        this.Register(contractType, implementationType, null, isSingleton);
                }
            }
        }
        #endregion
        //---------------------------------------------------------------------
        #region Resolve
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Resolve1"]/*'/>
        public TContract Resolve<TContract>()
        {
            return this.Resolve<TContract>(null as string);
        }
        //---------------------------------------------------------------------
#pragma warning disable 1574
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="Resolve3"]/*'/>
        public TContract Resolve<TContract>(params object[] args)
        {
            return this.Resolve<TContract>(null, false, args);
        }
#pragma warning restore 1574
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml'
        /// path='doc/entry[@name="Resolve4"]/*'/>
        public TContract Resolve<TContract>(string name, bool useDefaultIfNamedNotFound = false, params object[] args)
        {
            return (TContract)this.Resolve(typeof(TContract), name, useDefaultIfNamedNotFound, null, args);
        }
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="ResolveAll1"]/*'/>
        public IEnumerable<TContract> ResolveAll<TContract>()
        {
            return this.ResolveAll<TContract>(null);
        }
        //---------------------------------------------------------------------
#pragma warning disable 1574
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="ResolveAll2"]/*'/>
        public IEnumerable<TContract> ResolveAll<TContract>(params object[] args)
        {
            Type contractType = typeof(TContract);

            Components components = this.GetComponents(contractType.IsGenericType ?
                contractType.GetGenericTypeDefinition() :
                contractType);

            foreach (Component component in components)
                yield return (TContract)this.Resolve(contractType, component.Name, false, null, args);
        }
#pragma warning restore 1574
        #endregion
        //---------------------------------------------------------------------
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="GetRegisteredNamesForContract"]/*'/>
        public IEnumerable<string> GetRegisteredNamesForContract<TContract>()
        {
            Type contractType = typeof(TContract);

            Components components = this.GetComponents(contractType.IsGenericTypeDefinition ?
                contractType.GetGenericTypeDefinition() :
                contractType);

            return components.GetNames();
        }
        //---------------------------------------------------------------------
        #region Private Methods
        private void Register(Type contractType, Type implementationType, string name, bool isSingleton)
        {
            if (contractType       == null) throw new ArgumentNullException(nameof(contractType));
            if (implementationType == null) throw new ArgumentNullException(nameof(implementationType));

            this.ThrowIfDisposed();

            VerifyInterface(contractType, implementationType);

            this.RegisterCore(contractType, implementationType, name, isSingleton);
        }
        //---------------------------------------------------------------------
        private Components RegisterCore(Type contractType, Type implementationType, string name, bool isSingleton)
        {
            Components components = this.GetOrCreateComponents(contractType);
            Component component   = new Component(implementationType, name, isSingleton);

            components.AddComponent(component);

            return components;
        }
        //---------------------------------------------------------------------
        private static void VerifyInterface(Type contractType, Type implementationType)
        {
            if (contractType.IsAssignableFrom(implementationType))
                return;

            if (contractType.IsGenericType)
            {
                if (implementationType.GetInterfaces().Any(i =>
                    i.IsGenericType &&
                    i.GetGenericTypeDefinition() == contractType))
                    return;
            }

            throw new InvalidCastException(string.Format(
                CultureInfo.CurrentCulture,
                Resources.Strings.TypeIsNotAssignableFrom,
                contractType,
                implementationType));
        }
        //---------------------------------------------------------------------
        private Components GetOrCreateComponents(Type contractType)
        {
            /* Jede einzelne Operation ist threadsicher, aber nicht die 
             * Folge von Operationen!
             * Hier würde einfach der aktuelle Wert wieder (threadsicher)
             * überschrieben.
            if (!_container.ContainsKey(contractType))
                _container[contractType] = new Components();

            return _container[contractType];
            */

            Components value = _container.GetOrAdd(
                contractType,
                key => new Components(contractType));

            return value;
        }
        //---------------------------------------------------------------------
        private Components GetComponents(Type contractType)
        {
            if (_container.TryGetValue(contractType, out Components value))
                return value;

            return GetComponentsSlow();
            //-----------------------------------------------------------------
            Components GetComponentsSlow()
            {
                // Wird z.B. ein Parent per Resolve verlangt so ist der Typ
                // nicht registiert, daher den Typen suchen.
                foreach (var kvp in _container)
                    if (contractType.IsAssignableFrom(kvp.Key))
                        return kvp.Value;

                throw new InvalidOperationException(string.Format(
                    CultureInfo.CurrentCulture,
                    Resources.Strings.TypeIsNotRegistered,
                    contractType));
            }
        }
        //---------------------------------------------------------------------
        private object Resolve(Type contractType, string name, bool useDefaultIfNamedNotFound, Type notThisType, params object[] args)
        {
            this.ThrowIfDisposed();

            if (contractType.IsGenericType)
                return this.ResolveGeneric(contractType, name, useDefaultIfNamedNotFound, notThisType);

            return this.ResolveCore(contractType, name, useDefaultIfNamedNotFound, notThisType, args);
        }
        //---------------------------------------------------------------------
        private object ResolveCore(Type contractType, string name, bool useDefaultIfNamedNotFound, Type notThisType, params object[] args)
        {
            Components components = this.GetComponents(contractType);
            return this.ResolveCore(components, name, useDefaultIfNamedNotFound, notThisType, args);
        }
        //---------------------------------------------------------------------
        private object ResolveCore(Components components, string name, bool useDefaultIfNamedNotFound, Type notThisType, params object[] args)
        {
            Component component = components.GetComponent(name, useDefaultIfNamedNotFound, notThisType);
            return component.CreateImplementation(t => this.Resolve(t, null, useDefaultIfNamedNotFound, component.Type), args);
        }
        //---------------------------------------------------------------------
        private object ResolveGeneric(Type contractType, string name, bool useDefaultIfNamedNotFound, Type notThisType)
        {
            Components components;
            if (!_container.TryGetValue(contractType, out components) || !components.ContainsName(name))
                components = this.RegisterConcreteGenericType(contractType, name);

            return this.ResolveCore(components, name, useDefaultIfNamedNotFound, notThisType);
        }
        //---------------------------------------------------------------------
        private Components RegisterConcreteGenericType(Type contractType, string name)
        {
            Type genericDefinition  = contractType.GetGenericTypeDefinition();
            Type[] genericArguments = contractType.GetGenericArguments();

            if (!_container.ContainsKey(genericDefinition))
                throw new InvalidOperationException(string.Format(
                    CultureInfo.CurrentCulture,
                    Resources.Strings.GenericTypeIsNotRegistered,
                    contractType));

            Components components = _container[genericDefinition];
            Component component   = components.GetComponent(name, false);

            Type genericType = component.Type;

            if (genericType.IsGenericType)
                genericType = genericType.MakeGenericType(genericArguments);

            return this.RegisterCore(contractType, genericType, name, false);
        }
        #endregion
        //---------------------------------------------------------------------
        #region IDisposable Members
        /// <include 
        /// file='IoCContainer.doc.xml' 
        /// path='doc/entry[@name="DisposeCore"]/*'/>
        protected override void DisposeCore()
        {
            foreach (var kvp in _container)
            {
                if (kvp.Key == typeof(IoCContainer)) continue;
                kvp.Value.Dispose();
            }
        }
        #endregion
    }
}